window.addEventListener('load', function() {
    baguetteBox.run('.gallary');
});

const scrollBtn = document.getElementById('scrollToTop')

window.onscroll = () => {
    if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
        scrollBtn.style.display = 'block'
    } else {
        scrollBtn.style.display = 'none'
    }
}

let intervalId = 0

function scrollStep() {
    if (window.pageYOffset === 0) {
        clearInterval(intervalId);
    }
    window.scroll(0, window.pageYOffset - 50);
}

function scrollToTop() {
    intervalId = setInterval(scrollStep, 16.66);
}

scrollBtn.addEventListener('click', scrollToTop);